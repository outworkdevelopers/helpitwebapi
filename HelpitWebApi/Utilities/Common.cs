﻿using HelpitWebApi.ServiceData;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Utilities
{
    public class Common : BaseServiceData
    {
        public static bool CreateLog(string OrganizationId, int OrganizationIntId, string UserId, int UserIntId, string InputString, string Layer, string MethodName, string Exception)
        {
            var createLogStatus = "";
            try
            {
                using (var dbConnection = new MySqlConnection(DbConnectionString))
                {
                    using (var command = new MySqlCommand("logs_createlog", dbConnection) { CommandType = CommandType.StoredProcedure })
                    {
                        dbConnection.Open();
                        command.Parameters.AddWithValue("porganizationid", OrganizationId);
                        command.Parameters.AddWithValue("porganizationintid", OrganizationIntId);
                        command.Parameters.AddWithValue("puserid", UserId);
                        command.Parameters.AddWithValue("puserintid", UserIntId);
                        command.Parameters.AddWithValue("pinputstring", InputString);
                        command.Parameters.AddWithValue("player", Layer);
                        command.Parameters.AddWithValue("pmethodname", MethodName);
                        command.Parameters.AddWithValue("pexception", Exception);
                        createLogStatus = command.ExecuteNonQuery().ToString();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("CreateLog: Common Layer: Creating Log Failed", e);

                throw e;
            }
        }
    }
}