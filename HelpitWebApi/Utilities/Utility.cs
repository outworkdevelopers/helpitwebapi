﻿using HelpitWebApi.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace HelpitWebApi.Utilities
{
    public static class Utility
    {
        public static string NewId()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        public static string GetUserType(int userType)
        {
            var usertype = "";

            if (userType == 0) { usertype = "Admin"; }
            else if (userType == 1) { usertype = "Internal User"; }
            else if (userType == 2) { usertype = "Community"; }
            return usertype;
        }

        public static string GetIssueStatusName(int issueStatus)
        {
            var issueStatusName = "";

            if (issueStatus == 0) { issueStatusName = "Not Started"; }
            else if (issueStatus == 1) { issueStatusName = "Work In Progress"; }
            else if (issueStatus == 2) { issueStatusName = "Closed"; }
            return issueStatusName;
        }
        public static object DbNullParam(object parameterValue)
        {
            if (parameterValue == null)
                return "";
            return parameterValue;
        }

        public static object DbNullParamforInt(object parameterValue)
        {
            if (parameterValue == null)
                return "0";
            return parameterValue;
        }
        public static string Encrypt(string clearText)
        {
            const string encryptionKey = "CONNECTICA2014";
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor == null) return clearText;
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            const string encryptionKey = "CONNECTICA2014";
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor == null) return cipherText;
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
    public static class ApiUtilityExtensions
    {
        public static string GetHeaderValue(this HttpRequestMessage request, string name)
        {
            var found = request.Headers.TryGetValues(name, out var values);
            return found ? values.FirstOrDefault() : null;
        }
    }
    public static class DataReaderExtensions
    {
        public static string ReadStringValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];
            return coloumnObjectValue?.ToString();
        }

        public static int ReadIntegerValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToInt32(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static long ReadLongIntegerValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToInt64(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static decimal ReadDecimalValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToDecimal(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static double ReadDoubleValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToDouble(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static Boolean ReadBooleanValue(this MySqlDataReader dataReader, string coloumnName)
        {
            var coloumnObjectValue = dataReader[coloumnName];

            if (coloumnObjectValue == null)
                return false;
            try
            {
                return Convert.ToBoolean(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string ReadStringValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];
            return coloumnObjectValue?.ToString();
        }

        public static int ReadIntegerValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToInt32(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static long ReadLongIntegerValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToInt64(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static decimal ReadDecimalValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToDecimal(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static double ReadDoubleValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];

            if (coloumnObjectValue == null)
                return 0;
            try
            {
                return Convert.ToDouble(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static Boolean ReadBooleanValue(this IDataRecord dataRecord, string coloumnName)
        {
            var coloumnObjectValue = dataRecord[coloumnName];

            if (coloumnObjectValue == null)
                return false;
            try
            {
                return Convert.ToBoolean(coloumnObjectValue);
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    public static class ServiceResponseExtension
    {
        public static ServiceResponse SeResponse(string code, string status, string data, string description, string message, string helpurl)
        {
            var response = new ServiceResponse
            {
                Code = code,
                Data = data,
                Description = description,
                HelpUrl = helpurl,
                Message = message,
                Status = status
            };
            return response;
        }

        public static ServiceResponse SendAuthorizationFailureResponse()
        {
            return new ServiceResponse
            {
                Code = "401",
                Data = "",
                Description = "Authorization Failed",
                HelpUrl = "",
                Message = "Authorization Failed",
                Status = ServiceResponseCodes.Authfailure.ToString()
            };
        }

        public static ServiceResponse SendAuthorizationSuccessResponse()
        {
            return new ServiceResponse
            {
                Code = "200",
                Data = "",
                Description = "Authorization Success",
                HelpUrl = "",
                Message = "Authorization Success",
                Status = ServiceResponseCodes.Authsuccess.ToString()
            };
        }

        public static ServiceResponse SendInvalidDataInputResponse(string description = "Invalid Data", string message = "Please check the input data")
        {
            return new ServiceResponse
            {
                Code = ServiceResponseCodes.InvalidInputData.ToString(),
                Data = "",
                Description = description,
                HelpUrl = "",
                Message = message,
                Status = ServiceResponseCodes.InvalidInputData.ToString()
            };
        }


    }
}