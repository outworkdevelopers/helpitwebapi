﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.SignUp
{
    [RoutePrefix("webapp/v1/signup")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SignUprController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("")]
        [HttpPost]
        public ServiceResponse SignUp([FromBody] RegisterRequestObject registerRequestObject)
        {
            if (ModelState.IsValid)
            {
                return new SignUpServiceBase().Registration(registerRequestObject);
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return ServiceResponseExtension.SeResponse("VAL-250", "Failed", message, message, message, "");
            }
        }

        [Route("verifymail")]
        [HttpPost]
        public ServiceResponse VerifyEmail([FromUri] string email)
        {
            if (ModelState.IsValid)
            {
                return new SignUpServiceBase().VerifyEmail(email);
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return ServiceResponseExtension.SeResponse("VAL-250", "Failed", message, message, message, "");
            }
        }
    }

}
