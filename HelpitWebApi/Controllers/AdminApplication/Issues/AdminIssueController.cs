﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Issues
{
    [RoutePrefix("webapp/v1/admin/issue")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminIssueController : ApiController
    {

        public HttpResponseMessage Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return response;
        }

        [Route("")]
        [HttpPost]
        public ServiceResponse CreateIssueForAdmin([FromBody] IssueRequestObject issueRequest)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminIssueServiceBase().CreateIssueforAdmin(userToken, issueRequest) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("all")]
        [HttpGet]
        public ServiceResponse GetIssuesforAdmin()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminIssueServiceBase().GetIssuesforAdmin(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("issueid/{issueId}")]
        [HttpGet]
        public ServiceResponse GetIssueByIssueId([FromUri]int issueId)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminIssueServiceBase().GetIssueByIssueId(userToken, issueId) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("update")]
        [HttpPut]
        public ServiceResponse UpdateIssueStatusByIssueId([FromBody] UpdateIssueRequestObject updateIssueRequest)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminIssueServiceBase().UpdateIssueStatusByIssueId(userToken, updateIssueRequest.Id, updateIssueRequest.Status) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }
    }
}
