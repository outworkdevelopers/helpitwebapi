﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Faq
{

    [RoutePrefix("webapp/v1/admin/faq/category")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminFaqCategoryController : ApiController
    {
        [Route("create")]
        [HttpPost]
        public ServiceResponse CreateFaqCategory([FromBody]FaqCategoryInputRequest faqCategoryObject)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().CreateFaqCategory(userToken, faqCategoryObject) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("{faqcategoryintid}")]
        [HttpGet]
        public ServiceResponse GetFaqCategoryById([FromUri] int faqCategoryIntId)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().GetFaqCategoryById(userToken, faqCategoryIntId) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("all")]
        [HttpGet]
        public ServiceResponse GetFaqCategories()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().GetFaqCategories(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("{faqcategoryintid}/update")]
        [HttpPut]
        public ServiceResponse UpdateFaqCategory([FromUri] int FaqCategoryIntId, [FromBody] FaqCategoryInputRequest faqCategoryUpdate)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().UpdateFaqCategory(userToken, FaqCategoryIntId, faqCategoryUpdate) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("topic")]
        [HttpPost]
        public ServiceResponse CreateTopic([FromBody]FaqCategoryInputRequest faqCategoryObject)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().CreateTopic(userToken, faqCategoryObject) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("topic/{categoryintid}")]
        [HttpGet]
        public ServiceResponse GetTopicByParentId([FromUri]int categoryIntId)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqCategoryServiceBase().GetTopicByParentId(userToken, categoryIntId) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }
    }
}
