﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Faq
{
    [RoutePrefix("webapp/v1/admin/faq")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminFaqController : ApiController
    {
        [Route("")]
        [HttpPost]
        public ServiceResponse CreateFaq([FromBody]FaqInputObject faqObject)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqServiceBase().CreateFaq(userToken, faqObject) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("{faqintid}")]
        [HttpGet]
        public ServiceResponse GetFaqByFaqId([FromUri] string faqIntId)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqServiceBase().GetFaqByFaqId(userToken, faqIntId) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("all")]
        [HttpGet]
        public ServiceResponse GetFaqs([FromUri] int categoryIntId, [FromUri] int topicIntId)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqServiceBase().GetFaqs(userToken,categoryIntId,topicIntId) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("{faqintid}/update")]
        [HttpPut]
        public ServiceResponse UpdateFaq([FromUri] int FaqIntId, [FromBody] FaqInputObject faqUpdate)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminFaqServiceBase().UpdateFaq(userToken, FaqIntId, faqUpdate) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }
    }
}
