﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Requestor
{
    [RoutePrefix("webapp/v1/admin/requestor")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminRequestorController : ApiController
    {
        [Route("")]
        [HttpPost]
        public ServiceResponse CreateRequestorForAdmin([FromBody] RequestorRequestObject requestorRequest)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminRequestorServiceBase().CreateRequestorForAdmin(userToken, requestorRequest) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("all")]
        [HttpGet]
        public ServiceResponse GetRequestorsForAdmin()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminRequestorServiceBase().GetRequestorsForAdmin(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("requestorid/{requestorid}")]
        [HttpGet]
        public ServiceResponse GetRequestorByRequestorId([FromUri]int requestorid)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminRequestorServiceBase().GetRequestorByRequestorId(userToken, requestorid) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("update/requestordata/{requestorid}")]
        [HttpPut]
        public ServiceResponse UpdateRequestorByRequestorId([FromUri]int requestorid, [FromBody]UpdateRequestorObject updateRequestor)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new AdminRequestorServiceBase().UpdateRequestorByRequestorId(userToken, requestorid, updateRequestor) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }
    }
}
