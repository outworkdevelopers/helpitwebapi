﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Login
{
    [RoutePrefix("webapp/v1/login")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("")]
        [HttpPost]
        public ServiceResponse AccountLogin([FromBody] LoginRequestObject loginRequestObject)
        {
            if (ModelState.IsValid)
            {
                return new LoginServiceBase().LoginUser(loginRequestObject);
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return ServiceResponseExtension.SeResponse("VAL-250", "Failed", message, message, message, "");
            }
        }

        [Route("JWT")]
        [HttpPost]
        public ServiceResponse JWTlogin([FromBody] LoginRequestObject loginRequestObject)
        {
            if (ModelState.IsValid)
            {
                return new LoginServiceBase().JWTLoginUser(loginRequestObject);
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                return ServiceResponseExtension.SeResponse("VAL-250", "Failed", message, message, message, "");
            }
        }
    }
}
