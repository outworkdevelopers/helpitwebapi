﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.MasterList
{
    [RoutePrefix("webapp/v1/admin/masterlist")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterListController : ApiController
    {
        [Route("categories")]
        [HttpGet]
        public ServiceResponse GetCategories()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new MasterListServiceBase().GetCategories(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

        [Route("status")]
        [HttpGet]
        public ServiceResponse GetStatus()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new MasterListServiceBase().GetStatus(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


        [Route("channels")]
        [HttpGet]
        public ServiceResponse GetChannels()
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new MasterListServiceBase().GetChannels(userToken) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }

    }
}
