﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceBases;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HelpitWebApi.Controllers.AdminApplication.Source
{
    [RoutePrefix("channel/v1/source")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SourceController : ApiController
    {
        [Route("")]
        [HttpPost]
        public ServiceResponse CreateIssueForRequestor([FromBody] IssueEntity issueEntity)
        {
            var userToken = Request.GetHeaderValue("utoken");
            return userToken != null ? new SourceServiceBase().CreateIssueForRequestor(userToken, issueEntity) :
                ServiceResponseExtension.SendAuthorizationFailureResponse();
        }


    }
}
