﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class SourceServiceData : BaseServiceData
    {
        public int IsEmailExists(string email)
        {
            var id = 0;
            var checkEmailQuery = "select id from requestor where email=" + "'" + email + "'";
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand(checkEmailQuery, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            id = dataReader.ReadIntegerValue("id");
                        }
                    }
                }
            }
            return id;
        }

        public int IsPhoneExists(string phone)
        {
            var id = 0;
            var checkPhoneQuery = "select id from requestor where phoneno=" + "'" + phone + "'";
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand(checkPhoneQuery, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            id = dataReader.ReadIntegerValue("id");
                        }
                    }
                }
            }
            return id;
        }

    }
}