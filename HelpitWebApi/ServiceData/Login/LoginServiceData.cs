﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class LoginServiceData : BaseServiceData
    {
        public UserResponseObject LoginUser(string loginId, string password)
        {
            Log.Info("LoginUser: Login Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var userInfo = new UserResponseObject();
                using (var command = new MySqlCommand("user_login", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ploginid", Utility.DbNullParam(loginId));
                    command.Parameters.AddWithValue("@ppassword", Utility.Encrypt(Utility.DbNullParam(password).ToString()));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("Login Service Data Layer :LoginUser: Data Reader Entered");
                                userInfo = UserDataReader.ReadUserInfoFromDataReader(dataReader);
                                Log.Info("Login Service Data Layer :LoginUser: Data Reader Finished");
                            }
                        }
                        Log.Info("LoginUser: Login Service Data: Execution Finished");
                        return userInfo;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, loginId, 0, "", "Login Service Data", "LoginUser", e.Message);
                        Log.Error("LoginUser: Login Service Data: User Login Failed", e);
                        throw e;
                    }
                }
            }

        }
    }
}