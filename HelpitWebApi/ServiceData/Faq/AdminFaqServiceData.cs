﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class AdminFaqServiceData : BaseServiceData
    {
        public int CreateFaq(FaqInputObject faqEntity, int userIntId, string userId, int organizationIntId, string organizationId)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("faq_createfaq", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("pbotintid",faqEntity.BotIntId );
                        command.Parameters.AddWithValue("pbotid", faqEntity.BotId);
                        command.Parameters.AddWithValue("pcategoryintid", faqEntity.CategoryIntId);
                        command.Parameters.AddWithValue("pcategoryid", faqEntity.CategoryId);
                        command.Parameters.AddWithValue("ptopicintid", faqEntity.TopicIntId);
                        command.Parameters.AddWithValue("ptopicid", faqEntity.TopicId);
                        command.Parameters.AddWithValue("pprimaryquestion", faqEntity.PrimaryQuestion);
                        command.Parameters.AddWithValue("pvariantquestions", JsonConvert.SerializeObject(faqEntity.VariantQuestions));
                        command.Parameters.AddWithValue("presponse", faqEntity.Response);
                        command.Parameters.AddWithValue("pcreatedby", userId);
                        command.Parameters.AddWithValue("pmodifiedby", userId);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, userIntId, "", "Admin Faq Service Data", "CreateFaq", e.Message);
                        throw e;
                    }
                }
            }
        }

        public FaqInputObject GetFaqByFaqId(string faqIntId, int userIntId, int organizationIntId)
        {
            Log.Info("GetFaqByFaqId: Admin Faq Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var singleFaq = new FaqInputObject();
                using (var command = new MySqlCommand("faq_getfaqbyid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@pfaqintid", Utility.DbNullParamforInt(faqIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                               singleFaq = FaqDataReader.ReadFaqInfoFromDataReader(dataReader);
                            }
                        }
                        Log.Info("GetFaqByFaqId: Admin Faq Service Data: Execution Finished");
                        return singleFaq;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Admin Faq Service Data", "GetFaqByFaqId", e.Message);
                        Log.Error("GetFaqByFaqId: Admin Faq Service Data: GetFaqByFaqId Failed", e);
                        throw e;
                    }
                }

            }
        }

        public List<FaqInputObject> GetFaqs(int userIntId, int organizationIntId,int categoryIntId,int topicIntId)
        {
            Log.Info("GetFaqs: Admin Faq Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var faqsList = new List<FaqInputObject>();
                using (var command = new MySqlCommand("faq_getfaqs", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@porganizationintid", Utility.DbNullParamforInt(organizationIntId));
                    command.Parameters.AddWithValue("@pcategoryintid", Utility.DbNullParamforInt(categoryIntId));
                    command.Parameters.AddWithValue("@ptopicintid", Utility.DbNullParamforInt(topicIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleFaq = FaqDataReader.ReadFaqInfoFromDataReader(dataReader);
                                faqsList.Add(singleFaq);
                            }
                        }
                        Log.Info("GetFaqs: Admin Faq Service Data: Execution Finished");
                        return faqsList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Admin Faq Service Data", "GetFaqs", e.Message);
                        Log.Error("GetFaqs: Admin Faq Service Data: GetFaqs Failed", e);
                        throw e;
                    }
                }

            }
        }

        public bool UpdateFaq(int userIntId,string userId, int organizationIntId, int faqIntId, FaqInputObject faqUpdate)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("faq_updatefaq", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pprimaryquestion", faqUpdate.PrimaryQuestion);
                        command.Parameters.AddWithValue("pvariantquestions", JsonConvert.SerializeObject(faqUpdate.VariantQuestions));
                        command.Parameters.AddWithValue("presponse", faqUpdate.Response);
                        command.Parameters.AddWithValue("puserid", userId);
                        command.Parameters.AddWithValue("pfaqintid", faqIntId);
                        return Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Admin Faq Service Data", "UpdateFaq", e.Message);
                        throw e;
                    }
                }
            }
        }
    }
}