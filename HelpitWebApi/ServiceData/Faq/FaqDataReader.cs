﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class FaqDataReader
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static FaqInputObject ReadFaqInfoFromDataReader(IDataRecord dataReader)
        {
            Log.Info("Read FaqInfo From DataReader: Faq Data Reader : Execution Started");
            try
            {
                var singleFaqResponse = new FaqInputObject
                {
                     Id = dataReader.ReadIntegerValue("id"),
                     FaqId = dataReader.ReadStringValue("faqid"),
                     OrganizationIntId = dataReader.ReadIntegerValue("organizationintid"),
                     OrganizationId = dataReader.ReadStringValue("organizationid"),
                     BotIntId = dataReader.ReadIntegerValue("botintid"),
                     BotId = dataReader.ReadStringValue("botid"),
                     CategoryIntId = dataReader.ReadIntegerValue("categoryintid"),
                     CategoryId = dataReader.ReadStringValue("categoryid"),
                     TopicIntId = dataReader.ReadIntegerValue("topicintid"),
                     TopicId = dataReader.ReadStringValue("topicid"),
                     PrimaryQuestion = dataReader.ReadStringValue("primaryquestion"),
                     VariantQuestions = JsonConvert.DeserializeObject<string[]>(dataReader["variantquestions"].ToString()),
                     Response = dataReader.ReadStringValue("response"),
                     CreatedDate = dataReader.ReadStringValue("createddate"),
                     ModifiedDate = dataReader.ReadStringValue("modifieddate"),
                     CreatedBy = dataReader.ReadStringValue("createdby"),
                     ModifiedBy = dataReader.ReadStringValue("modifiedby"),
                    IsDeleted = dataReader.ReadBooleanValue("isdeleted")
                };
                Log.Info("Read FaqInfo From DataReader: Faq Data Reader : Execution Finished");
                return singleFaqResponse;
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        internal static FaqCategoryInputRequest ReadFaqCategoryInfoFromDataReader(IDataRecord dataReader)
        {
            Log.Info("Read FaqCategoryInfo From DataReader: Faq Data Reader : Execution Started");
            try
            {
                var singleFaqCategoryResponse = new FaqCategoryInputRequest
                {
                    Id = dataReader.ReadIntegerValue("id"),
                    CategoryId = dataReader.ReadStringValue("categoryid"),
                    OrganizationIntId = dataReader.ReadIntegerValue("organizationintid"),
                    OrganizationId = dataReader.ReadStringValue("organizationid"),
                    BotIntId = dataReader.ReadIntegerValue("botintid"),
                    BotId = dataReader.ReadStringValue("botid"),
                    ParentIntId = dataReader.ReadIntegerValue("parentintid"),
                    ParentId = dataReader.ReadStringValue("parentid"),
                    CategoryName = dataReader.ReadStringValue("categoryname"),
                    Description = dataReader.ReadStringValue("description"),
                    CreatedDate = dataReader.ReadStringValue("createddate"),
                    ModifiedDate = dataReader.ReadStringValue("modifieddate"),
                    CreatedBy = dataReader.ReadStringValue("createdby"),
                    ModifiedBy = dataReader.ReadStringValue("modifiedby"),
                    IsDeleted=dataReader.ReadBooleanValue("isdeleted")
                };
                Log.Info("Read FaqCategoryInfo From DataReader: Faq Data Reader : Execution Finished");
                return singleFaqCategoryResponse;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}