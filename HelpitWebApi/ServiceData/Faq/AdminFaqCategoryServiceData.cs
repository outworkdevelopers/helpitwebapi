﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class AdminFaqCategoryServiceData : BaseServiceData
    {
        public int CreateFaqCategory(FaqCategoryInputRequest faqCategoryObject, int userIntId, string userId, int organizationIntId, string organizationId)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("faqcategory_createcategory", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("pbotintid", faqCategoryObject.BotIntId);
                        command.Parameters.AddWithValue("pbotid", faqCategoryObject.BotId);
                        command.Parameters.AddWithValue("pparentintid", faqCategoryObject.ParentIntId);
                        command.Parameters.AddWithValue("pparentid", faqCategoryObject.ParentId);
                        command.Parameters.AddWithValue("pcategoryname", faqCategoryObject.CategoryName);
                        command.Parameters.AddWithValue("pdescription", faqCategoryObject.Description);
                        command.Parameters.AddWithValue("pcreatedby", userId);
                        command.Parameters.AddWithValue("pmodifiedby", userId);
                        command.Parameters.AddWithValue("pcategoryid", "");
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, userIntId, "", "Admin Faq Category Service Data", "CreateFaqCategory", e.Message);
                        throw e;
                    }
                }
            }
        }

        public FaqCategoryInputRequest GetFaqCategoryById(int faqCategoryIntId, int userIntId, int organizationIntId)
        {
            Log.Info("GetFaqCategoryById: Admin Faq Category Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var singleFaqCategory = new FaqCategoryInputRequest();
                using (var command = new MySqlCommand("faqcategory_getcatogorybyid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@pcategoryintid", Utility.DbNullParamforInt(faqCategoryIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                singleFaqCategory = FaqDataReader.ReadFaqCategoryInfoFromDataReader
                                    (dataReader);
                            }
                        }
                        Log.Info("GetFaqCategoryById: Admin Faq Category Service Data: Execution Finished");
                        return singleFaqCategory;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Admin Faq Category Service Data", "GetFaqCategoryById", e.Message);
                        Log.Error("GetFaqCategoryById: Admin Faq Category Service Data: GetFaqCategoryById Failed", e);
                        throw e;
                    }
                }

            }
        }

        public List<FaqCategoryInputRequest> GetFaqCategories(int userIntId, int organizationIntId)
        {
            Log.Info("GetFaqCategories: Admin Faq Category Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var faqCategoriesList = new List<FaqCategoryInputRequest>();
                using (var command = new MySqlCommand("faqcategory_getcategories", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@porganizationintid", Utility.DbNullParamforInt(organizationIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleFaqCategory = FaqDataReader.ReadFaqCategoryInfoFromDataReader(dataReader);
                                var subCategory = GetTopicByParentId(singleFaqCategory.Id, userIntId, organizationIntId);
                                singleFaqCategory.SubCategories = subCategory.ToArray();
                                faqCategoriesList.Add(singleFaqCategory);
                            }
                           
                        }
                        Log.Info("GetFaqCategories: Admin Faq Category Service Data: Execution Finished");
                        return faqCategoriesList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Admin Faq Category Service Data", "GetFaqCategories", e.Message);
                        Log.Error("GetFaqCategories: Admin Faq Category Service Data: GetFaqCategories Failed", e);
                        throw e;
                    }
                }

            }
        }

        public bool UpdateFaqCategory(int userIntId, string userId, int organizationIntId, int faqCategoryIntId, FaqCategoryInputRequest faqCategoryUpdate)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("faqcategory_updatecategory", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pcategoryname", faqCategoryUpdate.CategoryName);
                        command.Parameters.AddWithValue("pdescription", faqCategoryUpdate.Description);
                        command.Parameters.AddWithValue("pmodifiedby", userId);
                        command.Parameters.AddWithValue("pcategoryintid", faqCategoryIntId);
                        return Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Admin Faq Service Data", "UpdateFaq", e.Message);
                        throw e;
                    }
                }
            }
        }

        public int CreateTopic(FaqCategoryInputRequest faqCategoryObject, int userIntId, string userId, int organizationIntId, string organizationId)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("faqcategory_createtopic", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("pbotintid", faqCategoryObject.BotIntId);
                        command.Parameters.AddWithValue("pbotid", faqCategoryObject.BotId);
                        command.Parameters.AddWithValue("pparentintid", faqCategoryObject.ParentIntId);
                        command.Parameters.AddWithValue("pparentid", faqCategoryObject.ParentId);
                        command.Parameters.AddWithValue("pcategoryname", faqCategoryObject.CategoryName);
                        command.Parameters.AddWithValue("pdescription", faqCategoryObject.Description);
                        command.Parameters.AddWithValue("pcreatedby", userId);
                        command.Parameters.AddWithValue("pmodifiedby", userId);
                        command.Parameters.AddWithValue("pcategoryid", "");
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, userIntId, "", "Admin Faq Category Service Data", "CreateTopic", e.Message);
                        throw e;
                    }
                }
            }

        }

        public List<Topic> GetTopicByParentId(int parentIntId, int userIntId, int organizationIntId)
        {
            Log.Info("GetTopicByParentId: Admin Faq Category Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var topicList = new List<Topic>();
                using (var command = new MySqlCommand("faqcategory_gettopicbyparentid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@pparentintid", Utility.DbNullParamforInt(parentIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleTopicName = new Topic();
                                singleTopicName.CategoryName = dataReader.ReadStringValue("categoryname");
                                singleTopicName.ParentId= dataReader.ReadStringValue("parentid");
                                singleTopicName.ParentIntId= dataReader.ReadIntegerValue("parentintid");
                                singleTopicName.CategoryId = dataReader.ReadStringValue("categoryid");
                                singleTopicName.CategoryIntId = dataReader.ReadIntegerValue("id");
                                topicList.Add(singleTopicName);
                            }
                        }
                        Log.Info("GetTopicByParentId: Admin Faq Category Service Data: Execution Finished");
                        return topicList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Admin Faq Category Service Data", "GetTopicByParentId", e.Message);
                        Log.Error("GetTopicByParentId: Admin Faq Category Service Data: GetTopicByParentId Failed", e);
                        throw e;
                    }
                }

            }
        }
    }
}