﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class TemplateServiceData:BaseServiceData
    {
        internal Template GetTemplate(string mailType)
        {
            Log.Info("Get Template : Template Service Data: Execution Started");
            var singleTemplate = new Template();
            try
            {
                using (var dbConnection = new MySqlConnection(DbConnectionString))
                {
                    using (var command = new MySqlCommand("template_gettemplates", dbConnection) { CommandType = CommandType.StoredProcedure })
                    {
                        dbConnection.Open();
                        command.Parameters.AddWithValue("@pmailtype", mailType);
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                singleTemplate.HtmlTemplateBody = dataReader.ReadStringValue("customtext");
                                singleTemplate.CustomText = dataReader.ReadStringValue("customtext");
                                singleTemplate.Subject = dataReader.ReadStringValue("subject");
                                singleTemplate.TemplateBody = dataReader.ReadStringValue("body");
                                singleTemplate.OrganizationId = dataReader.ReadStringValue("organizationid");
                                singleTemplate.OrganizationIntId = dataReader.ReadIntegerValue("organizationintid");
                                singleTemplate.TemplateName = dataReader.ReadStringValue("templatename");
                                singleTemplate.TemplateId = dataReader.ReadStringValue("templateid");
                                singleTemplate.Id =dataReader.ReadIntegerValue("id");
                                singleTemplate.MailType = dataReader.ReadStringValue("mailtype");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            Log.Info("Get Template : Template Service Data: Execution Finished");
            return singleTemplate;
        }
    }
}