﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class MasterListServiceData : BaseServiceData
    {
        public List<CategoryResponseObject> GetCategories(int organizationIntId, int userIntId)
        {
            Log.Info("GetCategories: Master List Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var categoryList = new List<CategoryResponseObject>();
                using (var command = new MySqlCommand("category_getcategories", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@porganizationintid", Utility.DbNullParamforInt(organizationIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleCategory = new CategoryResponseObject();
                                singleCategory.Id = dataReader.ReadIntegerValue("id");
                                singleCategory.CategoryId = dataReader.ReadStringValue("categoryid");
                                singleCategory.OrganizationId = dataReader.ReadStringValue("organizationid");
                                singleCategory.OrganizationIntId = dataReader.ReadIntegerValue("organizationintid");
                                singleCategory.ParentId = dataReader.ReadStringValue("parentid");
                                singleCategory.ParentIntId = dataReader.ReadIntegerValue("parentintid");
                                singleCategory.CategoryName = dataReader.ReadStringValue("categoryname");
                                singleCategory.ObjectType = dataReader.ReadStringValue("objecttype");
                                singleCategory.CreatedBy = dataReader.ReadStringValue("createdby");
                                singleCategory.ModifiedBy = dataReader.ReadStringValue("modifiedby");
                                singleCategory.CreatedDate = dataReader.ReadStringValue("createddate");
                                singleCategory.ModifiedDate = dataReader.ReadStringValue("modifieddate");
                                singleCategory.IsDeleted = dataReader.ReadBooleanValue("isdeleted");
                                categoryList.Add(singleCategory);
                            }
                        }
                        Log.Info("GetCategories: Master List Service Data: Execution Finished");
                        return categoryList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Master List Service Data", "GetCategories", e.Message);
                        Log.Error("GetCategories: Master List Service Data: GetCategories Failed", e);
                        throw e;
                    }
                }

            }
        }

        public List<StatusResponseObject> GetStatus(int organizationIntId, int userIntId)
        {
            Log.Info("GetStatus: Master List Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var statusList = new List<StatusResponseObject>();
                using (var command = new MySqlCommand("status_getstatus", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@porganizationintid", Utility.DbNullParamforInt(organizationIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleStatus = new StatusResponseObject();
                                singleStatus.Id = dataReader.ReadIntegerValue("id");
                                singleStatus.StatusId = dataReader.ReadStringValue("statusid");
                                singleStatus.OrganizationId = dataReader.ReadStringValue("organizationid");
                                singleStatus.OrganizationIntId = dataReader.ReadIntegerValue("organizationintid");
                                singleStatus.ObjectType = dataReader.ReadStringValue("objecttype");
                                singleStatus.StatusName = dataReader.ReadStringValue("statusname");
                                singleStatus.CreatedBy = dataReader.ReadStringValue("createdby");
                                singleStatus.ModifiedBy = dataReader.ReadStringValue("modifiedby");
                                singleStatus.CreatedByInt = dataReader.ReadIntegerValue("createdbyint");
                                singleStatus.ModifiedByInt = dataReader.ReadIntegerValue("modifiedbyint");
                                singleStatus.CreatedDate = dataReader.ReadStringValue("createddate");
                                singleStatus.ModifiedDate = dataReader.ReadStringValue("modifieddate");
                                singleStatus.IsDeleted = dataReader.ReadBooleanValue("isdeleted");
                                statusList.Add(singleStatus);
                            }
                        }
                        Log.Info("GetStatus: Master List Service Data: Execution Finished");
                        return statusList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Master List Service Data", "GetStatus", e.Message);
                        Log.Error("GetStatus: Master List Service Data: GetStatus Failed", e);
                        throw e;
                    }
                }

            }
        }

        public List<ChannelResponseObject> GetChannels(int organizationIntId, int userIntId)
        {
            Log.Info("GetChannels: Master List Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                var channelQuery = "select * from channels";
                connection.Open();
                var channelsList = new List<ChannelResponseObject>();
                using (var command = new MySqlCommand(channelQuery, connection))
                {
                    command.CommandType = CommandType.Text;
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                var singleChannel = new ChannelResponseObject();
                                singleChannel.Id = dataReader.ReadIntegerValue("id");
                                singleChannel.ChannelId = dataReader.ReadStringValue("channelid");
                                singleChannel.ChannelName = dataReader.ReadStringValue("channelname");
                                singleChannel.ChannelType = dataReader.ReadIntegerValue("channeltype");
                                channelsList.Add(singleChannel);
                            }
                        }
                        Log.Info("GetChannels: Master List Service Data: Execution Finished");
                        return channelsList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Master List Service Data", "GetChannels", e.Message);
                        Log.Error("GetChannels: Master List Service Data: GetChannels Failed", e);
                        throw e;
                    }
                }

            }
        }
    }
}