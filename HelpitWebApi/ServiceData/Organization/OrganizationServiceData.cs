﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class OrganizationServiceData : BaseServiceData
    {
        public string CreateOrganization(string organizationId, string organizationName, string description, string url, int status, string createdBy, string modifiedBy)
        {

            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("organization_createorganization", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganiztionname", organizationName);
                        command.Parameters.AddWithValue("pdescription", description);
                        command.Parameters.AddWithValue("purl", url);
                        command.Parameters.AddWithValue("pstatus", status);
                        command.Parameters.AddWithValue("pcreatedby", createdBy);
                        command.Parameters.AddWithValue("pmodifiedby", modifiedBy);
                        return command.ExecuteScalar().ToString();
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, 0, "", 0, "", "Organization Service Data", "CreateOrganization", e.Message);
                        throw e;
                    }
                }
            }
        }
        public OrganizationResponseObject GetOrganizationByUserId(int userIntId)
        {
            Log.Info("GetOrganizationByUserId: Organization Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var orgInfo = new OrganizationResponseObject();
                using (var command = new MySqlCommand("organization_getuserorganizationbyuserid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@puserintid", Utility.DbNullParamforInt(userIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                orgInfo.Id = dataReader.ReadIntegerValue("id");
                                orgInfo.OrganizationId = dataReader.ReadStringValue("organizationid");
                                orgInfo.OrganizationName = dataReader.ReadStringValue("organizationname");
                                orgInfo.Description = dataReader.ReadStringValue("description");
                                orgInfo.Url = dataReader.ReadStringValue("url");
                                orgInfo.Status = dataReader.ReadIntegerValue("status");
                                orgInfo.CreatedBy = dataReader.ReadStringValue("createdby");
                                orgInfo.ModifiedBy = dataReader.ReadStringValue("modifiedby");
                                orgInfo.CreatedDate = dataReader.ReadStringValue("createddate");
                                orgInfo.ModifiedDate = dataReader.ReadStringValue("modifieddate");
                            }
                        }
                        Log.Info("GetOrganizationByUserId: Organization Service Data: Execution Finished");
                        return orgInfo;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, "", userIntId, "", "Organization Service Data", "GetOrganizationByUserId", e.Message);
                        Log.Error("GetOrganizationByUserId: Organization Service Data: GetOrganizationByUserId Failed", e);
                        throw e;
                    }
                }

            }
        }
    }
}