﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class RequestorServiceData : BaseServiceData
    {
        public int CreateRequestorForAdmin(RequestorRequestObject requestorRequest, int userIntId, string userId, int organizationIntId, string organizationId)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("requestor_createrequestor", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("prequestorid", "");
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("puserintid", userIntId);
                        command.Parameters.AddWithValue("puserid", userId);
                        command.Parameters.AddWithValue("prequestortype", requestorRequest.RequestorType);
                        command.Parameters.AddWithValue("prequestorname", requestorRequest.RequestorName);
                        command.Parameters.AddWithValue("pemail", requestorRequest.Email);
                        command.Parameters.AddWithValue("pphone", requestorRequest.PhoneNo);
                        command.Parameters.AddWithValue("pwatsappenabled", requestorRequest.WatsappEnabled);
                        command.Parameters.AddWithValue("pstatus", requestorRequest.Status);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, userIntId, "", "Requestor Service Data", "CreateRequestorForAdmin", e.Message);
                        throw e;
                    }
                }
            }
        }

        public List<RequestorResponseObject> GetRequestors(int userIntId, int organizationIntId)
        {
            Log.Info("GetRequestors: Requestor Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var requestorList = new List<RequestorResponseObject>();
                using (var command = new MySqlCommand("requestor_admin_getallrequestors", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@puserintid", Utility.DbNullParamforInt(userIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("Requestor Service Data Layer :GetRequestors: Data Reader Entered");
                                var singleRequestor = RequestorDataReader.ReadRequestorInfoFromDataReader(dataReader);
                                requestorList.Add(singleRequestor);
                                Log.Info("Requestor Service Data Layer :GetRequestors: Data Reader Finished");
                            }
                        }
                        Log.Info("GetRequestors: Requestor Service Data: Execution Finished");
                        return requestorList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Requestor Service Data", "GetRequestors", e.Message);
                        Log.Error("GetRequestors: Requestor Service Data: GetRequestors Failed", e);
                        throw e;
                    }
                }

            }
        }

        public RequestorResponseObject GetRequestorByRequestorId(int requestorIntId, int userIntId, int organizationIntId)
        {
            Log.Info("GetRequestorByRequestorId: Requestor Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var singleRequestor = new RequestorResponseObject();
                using (var command = new MySqlCommand("requestor_getrequestorbyrequestorid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@prequestorintid", Utility.DbNullParamforInt(requestorIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("Requestor Service Data Layer :GetRequestorByRequestorId: Data Reader Entered");
                                singleRequestor = RequestorDataReader.ReadRequestorInfoFromDataReader(dataReader);
                                Log.Info("Requestor Service Data Layer :GetRequestorByRequestorId: Data Reader Finished");
                            }
                        }
                        Log.Info("GetRequestorByRequestorId: Requestor Service Data: Execution Finished");
                        return singleRequestor;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Requestor Service Data", "GetRequestorByRequestorId", e.Message);
                        Log.Error("GetRequestorByRequestorId: Requestor Service Data: GetRequestorByRequestorId Failed", e);
                        throw e;
                    }
                }

            }
        }

        public bool UpdateRequestorByRequestorId(int requestorIntId, int userIntId, string userId, int organizationIntId, UpdateRequestorObject updateRequestor)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("requestor_update_requestordata", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("prequestortype", updateRequestor.RequestorType);
                        command.Parameters.AddWithValue("prequestorname", updateRequestor.RequestorName);
                        command.Parameters.AddWithValue("puserid", userId);
                        command.Parameters.AddWithValue("pemail", updateRequestor.Email);
                        command.Parameters.AddWithValue("pphone", updateRequestor.PhoneNo);
                        command.Parameters.AddWithValue("pwatsappenabled", updateRequestor.WatsappEnabled);
                        command.Parameters.AddWithValue("pstatus", updateRequestor.Status);
                        command.Parameters.AddWithValue("prequestorintid", requestorIntId);
                        return Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Requestor Service Data", "UpdateRequestorByRequestorId", e.Message);
                        throw e;
                    }
                }
            }
        }

    }
}