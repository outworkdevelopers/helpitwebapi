﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class RequestorDataReader
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static RequestorResponseObject ReadRequestorInfoFromDataReader(IDataRecord dataReader)
        {
            Log.Info("Read RequestorInfo From DataReader: Requestor Data Reader : Execution Started");
            try
            {
                var singleIssueResponse = new RequestorResponseObject
                {
                    Id = dataReader.ReadIntegerValue("id"),
                    RequestorId = dataReader.ReadStringValue("requestorid"),
                    OrganizationIntId = dataReader.ReadIntegerValue("organizationintid"),
                    OrganizationId = dataReader.ReadStringValue("organizationid"),
                    UserIntId = dataReader.ReadIntegerValue("userintid"),
                    UserId = dataReader.ReadStringValue("userid"),
                    RequestorType = dataReader.ReadIntegerValue("requestortype"),
                    RequestorName = dataReader.ReadStringValue("requestorname"),
                    Email = dataReader.ReadStringValue("email"),
                    PhoneNo = dataReader.ReadStringValue("phoneno"),
                    WatsappEnabled = dataReader.ReadIntegerValue("watsappenabled"),
                    Status = dataReader.ReadIntegerValue("status"),
                    CreatedBy = dataReader.ReadStringValue("createdby"),
                    CreatedDate = dataReader.ReadStringValue("createddate"),
                    ModifiedBy = dataReader.ReadStringValue("modifiedby"),
                    ModifiedDate = dataReader.ReadStringValue("modifieddate"),
                    IsDeleted = dataReader.ReadBooleanValue("isdeleted")


                };
                Log.Info("Read RequestorInfo From DataReader: Requestor Data Reader : Execution Finished");
                return singleIssueResponse;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}