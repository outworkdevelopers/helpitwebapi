﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class IssueServiceData : BaseServiceData
    {
        public int CreateIssue(IssueRequestObject issueRequest, int userIntId, string userId, string organizationId, int organizationIntId)
        {
            Log.Info("CreateIssue: Issue Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("issue_createissue", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pissueid", "");
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("pparentid", issueRequest.ParentId);
                        command.Parameters.AddWithValue("pparentintid", issueRequest.ParentIntId);
                        command.Parameters.AddWithValue("ptitle", issueRequest.Title);
                        command.Parameters.AddWithValue("pdescription", issueRequest.Description);
                        command.Parameters.AddWithValue("presolutionnotes", issueRequest.ResolutionNotes);
                        command.Parameters.AddWithValue("presolutiondate", issueRequest.ResolutionDate);
                        command.Parameters.AddWithValue("pcategoryintid", issueRequest.CategoryIntId);
                        command.Parameters.AddWithValue("pcategoryid", issueRequest.CategoryId);
                        command.Parameters.AddWithValue("psubcategoryintid", issueRequest.SubCategoryIntId);
                        command.Parameters.AddWithValue("psubcategoryid", issueRequest.SubCategoryId);
                        command.Parameters.AddWithValue("prequestorintid", issueRequest.RequestorIntId);
                        command.Parameters.AddWithValue("prequestorid", issueRequest.RequestorId);
                        command.Parameters.AddWithValue("psource", issueRequest.Source);
                        command.Parameters.AddWithValue("ppriority", issueRequest.Priority);
                        command.Parameters.AddWithValue("ptags", issueRequest.Tags);
                        command.Parameters.AddWithValue("pownerintid", userIntId);
                        command.Parameters.AddWithValue("pownerid", userId);
                        command.Parameters.AddWithValue("passignedtointid", userIntId);
                        command.Parameters.AddWithValue("passignedtoid", userId);
                        command.Parameters.AddWithValue("pusertype", issueRequest.UserType);
                        command.Parameters.AddWithValue("pstatus", issueRequest.Status);
                        command.Parameters.AddWithValue("pobjectintid", issueRequest.ObjectIntId);
                        command.Parameters.AddWithValue("pobjectid", issueRequest.ObjectId);
                        command.Parameters.AddWithValue("pobjecttype", issueRequest.ObjectType);
                        command.Parameters.AddWithValue("pcreatedby", issueRequest.CreatedBy);
                        command.Parameters.AddWithValue("pmodifiedby", issueRequest.ModifiedBy);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, userIntId, "", "Issue Service Data", "CreateIssue", e.Message);
                        Log.Info("CreateIssue: Issue Service Data : Execution Finished");
                        throw e;
                    }
                }
            }
        }

        public List<IssueResponseObject> GetIssues(int organizationIntId, int userIntId,string userId,string userName)
        {
            Log.Info("GetIssues: Issue Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var issuesList = new List<IssueResponseObject>();
                using (var command = new MySqlCommand("issue_admin_getallissues", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@porganizationintid", Utility.DbNullParamforInt(organizationIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("Issue Service Data Layer :GetIssues: Data Reader Entered");
                                var singleIssue = IssueDataReader.ReadIssueInfoFromDataReader(dataReader);
                                singleIssue.IssueUserType = Utility.GetUserType(singleIssue.UserType);
                                singleIssue.IssueStatus = Utility.GetIssueStatusName(singleIssue.Status);
                                if (singleIssue.CreatedBy== userId)
                                {
                                    singleIssue.CreatedByName = userName;
                                }
                                else
                                {
                                    var singleRequestor = GetIssueByIssueId(organizationIntId,userIntId,singleIssue.Id);
                                    singleIssue.CreatedByName = singleRequestor.RequestorName;
                                }
                                   
                                issuesList.Add(singleIssue);
                                Log.Info("Issue Service Data Layer :GetIssues: Data Reader Finished");
                            }
                        }
                        Log.Info("GetIssues: Issue Service Data: Execution Finished");
                        return issuesList;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Issue Service Data", "GetIssues", e.Message);
                        Log.Error("GetIssues: Issue Service Data: GetIssues Failed", e);
                        throw e;
                    }
                }

            }
        }

        public IssueResponseObject GetIssueByIssueId(int organizationIntId, int userIntId, int issueIntId)
        {
            Log.Info("GetIssueByIssueId: Issue Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var issueInfo = new IssueResponseObject();
                using (var command = new MySqlCommand("issue_getissuebyissueid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@pissueintid", Utility.DbNullParamforInt(issueIntId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("Issue Service Data Layer :GetIssueByIssueId: Data Reader Entered");
                                issueInfo = IssueDataReader.ReadIssueInfoFromDataReader(dataReader);
                                issueInfo.IssueUserType = Utility.GetUserType(issueInfo.UserType);
                                issueInfo.IssueStatus = Utility.GetIssueStatusName(issueInfo.Status);
                                Log.Info("Issue Service Data Layer :GetIssueByIssueId: Data Reader Finished");
                            }
                        }
                        Log.Info("GetIssueByIssueId: Issue Service Data: Execution Finished");
                        return issueInfo;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, "", userIntId, "", "Issue Service Data", "GetIssueByIssueId", e.Message);
                        Log.Error("GetIssueByIssueId: Issue Service Data: GetIssueByIssueId Failed", e);
                        throw e;
                    }
                }

            }
        }

        public bool UpdateIssueStatusByIssueId(int issueIntId, int status, int userIntId, string userId, int organizationIntId)
        {
            Log.Info("UpdateIssueStatusByIssueId: Issue Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("issue_update_issuestatusbyissueid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pissueintid", Utility.DbNullParamforInt(issueIntId));
                        command.Parameters.AddWithValue("pstatus", Utility.DbNullParamforInt(status));
                        command.Parameters.AddWithValue("puserid", Utility.DbNullParam(userId));
                        return Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Issue Service Data", "UpdateIssueStatusByIssueId", e.Message);
                        Log.Info("UpdateIssueByIssueId: Issue Service Data : Execution Finished");
                        throw e;
                    }
                }
            }
        }

        #region Ow_Status

        public int CreateIssueStatus(string objectId, int objectIntId, string objectType, string statusName, int userIntId, string userId, int organizationIntId)
        {
            Log.Info("CreateIssueStatus: Issue Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("status_createissuestatus", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pstatusid", "");
                        command.Parameters.AddWithValue("pobjectid", objectId);
                        command.Parameters.AddWithValue("pobjectintid", objectIntId);
                        command.Parameters.AddWithValue("pobjecttype", objectType);
                        command.Parameters.AddWithValue("pstatusname", statusName);
                        command.Parameters.AddWithValue("puserid", userId);
                        command.Parameters.AddWithValue("puserintid", userIntId);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", organizationIntId, userId, userIntId, "", "Issue Service Data", "CreateIssueStatus", e.Message);
                        Log.Info("CreateIssueStatus: Issue Service Data : Execution Finished");
                        throw e;
                    }
                }
            }
        }

        #endregion
    }
}