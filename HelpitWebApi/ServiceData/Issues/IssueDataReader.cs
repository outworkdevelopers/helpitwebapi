﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class IssueDataReader
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static IssueResponseObject ReadIssueInfoFromDataReader(IDataRecord dataReader)
        {
            Log.Info("Read IssueInfo From DataReader: Issue Data Reader : Execution Started");
            try
            {
                var singleIssueResponse = new IssueResponseObject
                {
                    Id = dataReader.ReadIntegerValue("id"),
                    IssueId = dataReader.ReadStringValue("issueid"),
                    OrganizationIntId = dataReader.ReadIntegerValue("organizationintid"),
                    OrganizationId = dataReader.ReadStringValue("organizationid"),
                    ParentIntId = dataReader.ReadIntegerValue("parentintid"),
                    ParentId = dataReader.ReadStringValue("parentid"),
                    Title = dataReader.ReadStringValue("title"),
                    Description = dataReader.ReadStringValue("description"),
                    ResolutionNotes = dataReader.ReadStringValue("resolutionnotes"),
                    ResolutionDate = dataReader.ReadStringValue("resolutiondate"),
                    CategoryIntId = dataReader.ReadIntegerValue("categoryintid"),
                    CategoryId = dataReader.ReadStringValue("categoryid"),
                    SubCategoryIntId = dataReader.ReadIntegerValue("subcategoryintid"),
                    SubCategoryId = dataReader.ReadStringValue("subcategoryid"),
                    RequestorIntId = dataReader.ReadIntegerValue("requestorintid"),
                    RequestorId = dataReader.ReadStringValue("requestorid"),
                    RequestorName = dataReader.ReadStringValue("requestorname"),
                    Source = dataReader.ReadStringValue("source"),
                    Priority = dataReader.ReadStringValue("priority"),
                    Tags = dataReader.ReadStringValue("tags"),
                    OwnerIntId = dataReader.ReadIntegerValue("ownerintid"),
                    OwnerId = dataReader.ReadStringValue("ownerid"),
                    AssignedToIntId = dataReader.ReadIntegerValue("assignedtointid"),
                    AssignedToId = dataReader.ReadStringValue("assignedtoid"),
                    UserType = dataReader.ReadIntegerValue("usertype"),
                    Status = dataReader.ReadIntegerValue("status"),
                    ObjectIntId = dataReader.ReadIntegerValue("objectintid"),
                    ObjectId = dataReader.ReadStringValue("objectid"),
                    ObjectType = dataReader.ReadStringValue("objecttype"),
                    CreatedBy = dataReader.ReadStringValue("createdby"),
                    CreatedDate = dataReader.ReadStringValue("createddate"),
                    ModifiedBy = dataReader.ReadStringValue("modifiedby"),
                    ModifiedDate = dataReader.ReadStringValue("modifieddate"),
                    IsDeleted = dataReader.ReadBooleanValue("isdeleted")
                };
                Log.Info("Read IssueInfo From DataReader: Issue Data Reader : Execution Finished");
                return singleIssueResponse;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}