﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class UserServiceData : BaseServiceData
    {
        public int CreateUser(string userId, string organizationId, int organizationIntId, string externalId, string userName, string loginId,
            string password, int userType, int status)
        {

            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("user_createuser", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("puserid", userId);
                        command.Parameters.AddWithValue("porganizationid", organizationId);
                        command.Parameters.AddWithValue("porganizationintid", organizationIntId);
                        command.Parameters.AddWithValue("pexternalid", externalId);
                        command.Parameters.AddWithValue("pusername", userName);
                        command.Parameters.AddWithValue("ploginid", loginId);
                        command.Parameters.AddWithValue("ppassword", Utility.Encrypt(password));
                        command.Parameters.AddWithValue("pusertype", userType);
                        command.Parameters.AddWithValue("pstatus", status);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog(organizationId, organizationIntId, userId, 0, "", "User Service Data", "CreateUser", e.Message);
                        throw e;
                    }
                }
            }
        }
        public UserResponseObject GetUserByUserId(string userId)
        {
            Log.Info("GetUserByUserId: User Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var userInfo = new UserResponseObject();
                using (var command = new MySqlCommand("user_getuserbyid", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@puserid", Utility.DbNullParam(userId));
                    try
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                Log.Info("User Service Data Layer :GetUserByUserId: Data Reader Entered");
                                userInfo = UserDataReader.ReadUserInfoFromDataReader(dataReader);
                                Log.Info("User Service Data Layer :GetUserByUserId: Data Reader Finished");
                            }
                        }
                        Log.Info("GetUserByUserId: User Service Data: Execution Finished");
                        return userInfo;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, userId, 0, "", "User Service Data", "GetUserByUserId", e.Message);
                        Log.Error("GetUserByUserId: User Service Data: GetUserByUserId Failed", e);
                        throw e;
                    }
                }

            }
        }

        internal bool IsUserExists(string loginId)
        {
            Log.Info("IsUserExists: User Service Data : Execution Started");
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                var logincount = 0;
                using (var command = new MySqlCommand("user_isuseravailable", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ploginid", Utility.DbNullParam(loginId));
                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                logincount = Convert.ToInt32(reader["logincount"].ToString());
                            }
                        }
                        Log.Info("IsUserExists: User Service Data: Execution Finished");
                        return logincount > 0;
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, loginId, 0, "", "User Service Data", "IsUserExists", e.Message);
                        Log.Error("IsUserExists: User Service Data: IsUserExists Failed", e);
                        throw e;
                    }
                }
            }
        }

        public int CreateUserToken(int objectIntId, string objectId, string objectType, string accessToken, int tokenType, string expiryDate)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand("usertoken_createtoken", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.AddWithValue("pobjectintid", objectIntId);
                        command.Parameters.AddWithValue("pobjectid", objectId);
                        command.Parameters.AddWithValue("pobjecttype", objectType);
                        command.Parameters.AddWithValue("paccesstoken", accessToken);
                        command.Parameters.AddWithValue("ptokentype", tokenType);
                        command.Parameters.AddWithValue("pexpirydate", expiryDate);
                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, objectId, objectIntId, "", "User Service Data", "CreateUserToken", e.Message);
                        throw e;
                    }
                }
            }
        }

        public bool UpdateLastLoginDate(string userId)
        {
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                var updateLastloginDateQuery = "update user set lastlogindate=UNIX_TIMESTAMP() where userid=" + "'" + userId + "'";
                connection.Open();
                using (var command = new MySqlCommand(updateLastloginDateQuery, connection))
                {
                    command.CommandType = CommandType.Text;
                    try
                    {
                        return Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                    catch (Exception e)
                    {
                        Common.CreateLog("", 0, "", 0, "", "User Service Data", "UpdateLastLoginDate", e.Message);
                        throw e;
                    }
                }
            }
        }

    }
}