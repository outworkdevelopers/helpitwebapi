﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class UserDataReader
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static UserResponseObject ReadUserInfoFromDataReader(IDataRecord dataReader)
        {
            Log.Info("Read UserInfo From DataReader: User Data Reader : Execution Started");
            try
            {
                var singleUserResponse = new UserResponseObject
                {
                    Id = dataReader.ReadIntegerValue("id"),
                    UserId = dataReader.ReadStringValue("userid"),
                    OrganizationIntId = dataReader.ReadIntegerValue("organizationintid"),
                    OrganizationId = dataReader.ReadStringValue("organizationid"),
                    ExternalId = dataReader.ReadStringValue("externalid"),
                    UserName = dataReader.ReadStringValue("username"),
                    LoginId = dataReader.ReadStringValue("loginid"),
                    UserType = dataReader.ReadIntegerValue("usertype"),
                    Status = dataReader.ReadIntegerValue("status"),
                    CreatedBy = dataReader.ReadStringValue("createdby"),
                    ModifiedBy = dataReader.ReadStringValue("modifiedby"),
                    CreatedDate = dataReader.ReadStringValue("createddate"),
                    ModifiedDate = dataReader.ReadStringValue("modifieddate"),
                    LastLoginDate = dataReader.ReadStringValue("lastlogindate"),


                };
                Log.Info("Read UserInfo From DataReader: User Data Reader : Execution Finished");
                return singleUserResponse;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}