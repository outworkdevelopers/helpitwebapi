﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceData
{
    public class BaseServiceData
    {
        internal static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static readonly string DbConnectionString = ConfigurationManager.ConnectionStrings["helpitConnectionString"].ConnectionString;

    }
}