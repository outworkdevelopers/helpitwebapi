﻿using HelpitWebApi.Entities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Web;

namespace HelpitWebApi
{
    public class TokenManager
    {
        private static string Secret = "B2B20417CA624035C4680954C7739F687D3B9E4DF85261A5C435003473B9E3BE";
        public static string GetJWTAuthenticationTicket(UserResponseObject objUser)
        {
            string token;
            if (!string.IsNullOrEmpty(objUser.UserId))
            {
                token = objUser.UserId;
            }
            else
            {
                return "";
            }
            if (token!="")
            {
                byte[] key = Convert.FromBase64String(Secret);
                SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
                SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[] {
                      new Claim(ClaimTypes.Name, token)}),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    SigningCredentials = new SigningCredentials(securityKey,
                    SecurityAlgorithms.HmacSha256Signature)
                };

                var handler = new JwtSecurityTokenHandler();
                var encrytedTicket = handler.CreateJwtSecurityToken(descriptor);
                var uToken= handler.WriteToken(encrytedTicket);
                return uToken;
            }
            return "";
    
        }
        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;
                var key = Convert.FromBase64String(Secret);
                var parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token,
                      parameters, out securityToken);
                return principal;
            }
            catch(Exception e)
            {
               throw e;
            }
        }
        public static string ValidateToken(string token)
        {
            string userId = null;
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            Claim userIdClaim = identity.FindFirst(ClaimTypes.Name);
            userId = userIdClaim.Value;
            return userId;
        }

    //    public RefreshToken GenerateRefreshToken(User user)
    //    {
    //        // Create the refresh token
    //        RefreshToken refreshToken = new RefreshToken()
    //        {
    //            Token = GenerateRefreshToken(),
    //            Expiration = DateTime.UtcNow.AddMinutes(35) // Make this configurable
    //        };

    //// Add it to the list of of refresh tokens for the user
    //        user.RefreshTokens.Add(refreshToken);

    //        // Update the user along with the new refresh token
    //        UserRepository.Update(user);

    //        return refreshToken;
    //    }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

    }
}  
