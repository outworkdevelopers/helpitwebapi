﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class RegisterRequestObject
    {
        private string _password;
        public string UserId { get; set; }
        public int UserIntId { get; set; }
        public string OrganizationId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationName { get; set; }
        //[Validators(ErrorMessage = "LoginId is Required")]
        public string LoginId { get; set; }
        //[Validators(ErrorMessage = "FirstName is Required")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password
        {
            get
            {
                return string.IsNullOrEmpty(_password) ? "password" : _password;
            }
            set { _password = value; }
        }
        public int UserType { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string Url { get; set; }
        public RegisterRequestObject()
        {

        }
    }

}