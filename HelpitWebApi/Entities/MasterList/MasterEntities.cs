﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class Status
    {
        public int Id { get; set; }
        public string StatusId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public string ObjectType { get; set; }
        public string StatusName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int CreatedByInt { get; set; }
        public int ModifiedByInt { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class StatusResponseObject : Status
    {

    }

    public class Category
    {
        public int Id { get; set; }
        public string CategoryId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public int ParentIntId { get; set; }
        public string ParentId { get; set; }
        public string ObjectType { get; set; }
        public string CategoryName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class CategoryResponseObject : Category
    {

    }

    public class Channel
    {
        public int Id { get; set; }
        public string ChannelId { get; set; }
        public string ChannelName { get; set; }
        public int ChannelType { get; set; }
    }

    public class ChannelResponseObject : Channel
    {

    }
}