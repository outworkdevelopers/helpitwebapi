﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class MailRestResponse
    {
        public string Content { get; set; }
        public string ContentEncoding { get; set; }
        public long ContentLength { get; set; }
        public string ContentType { get; set; }
        public Exception ErrorException { get; set; }
        public string ErrorMessage { get; set; }
        public byte[] RawBytes { get; set; }
        public string ResponseStatus { get; set; }
        public Uri ResponseUri { get; set; }
        public string Server { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string HtmlBody { get; set; }
        public string MessageId { get; set; }
    }

    public class Mailattachment
    {
        public string Path { get; set; }
    }
}