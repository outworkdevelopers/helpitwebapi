﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{


    public class LoginRequestObject
    {
        //[Validators(ErrorMessage = "LoginId is Required")]
        public string LoginId { get; set; }
        // [Validators(ErrorMessage = "Password is Required")]
        public string Password { get; set; }
    }
    public class LoginResponseObject : UserResponseObject
    {
        public string UserToken { get; set; }
        public string LoginId { get; set; }
        public string LoginType { get; set; }
        public OrganizationResponseObject OrganizationInfo { get; set; }
        public LoginResponseObject()
        {
            OrganizationInfo = new OrganizationResponseObject();
        }
    }


}