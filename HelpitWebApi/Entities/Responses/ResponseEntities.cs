﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class ServiceResponse
    {
        public string Code { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
        public string HelpUrl { get; set; }
    }

    public enum ServiceResponseCodes
    {
        Authfailure,
        InvalidInputData,
        Authsuccess
    }
}