﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class FaqEntity
    {
        public int Id { get; set; }
        public string FaqId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public int BotIntId { get; set; }
        public string BotId { get; set; }
        public int CategoryIntId { get; set; }
        public string CategoryId { get; set; }
        public int TopicIntId { get; set; }
        public string TopicId { get; set; }
        public string PrimaryQuestion { get; set; }
        public string[] VariantQuestions { get; set; }
        public string Response { get; set; }
        public string CreatedDate{ get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class FaqInputObject:FaqEntity
    {

    }

    public class FaqCategory
    {
        public int Id { get; set; }
        public string CategoryId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public int BotIntId { get; set; }
        public string BotId { get; set; }
        public int ParentIntId { get; set; }
        public string ParentId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public Topic[] SubCategories { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class FaqCategoryInputRequest:FaqCategory
    {

    }

    public class Topic
    {
        public string CategoryName { get; set; }
        public int ParentIntId { get; set; }
        public string ParentId { get; set; }
        public int CategoryIntId { get; set; }
        public string CategoryId { get; set; }
    }
}