﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class Template
    {
        public string OrganizationId { get; set; }
        public int OrganizationIntId { get; set; }
        public string TemplateName { get; set; }
        public string MailType { get; set; }
        public string Subject { get; set; }
        public string TemplateBody { get; set; }
        public string HtmlTemplateBody { get; set; }
        public string CustomText { get; set; }
        public string TemplateId { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public int Id { get; set; }
    }
}