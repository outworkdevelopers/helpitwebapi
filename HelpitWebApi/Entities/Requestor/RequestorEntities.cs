﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class Requestor
    {
        public int Id { get; set; }
        public string RequestorId { get; set; }
        public int OrganizationIntId { get; set; }
        public int UserIntId { get; set; }
        public string OrganizationId { get; set; }
        public string UserId { get; set; }
        public int RequestorType { get; set; }
        public string RequestorName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public int WatsappEnabled { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class RequestorRequestObject : Requestor
    {

    }
    public class RequestorResponseObject : Requestor
    {

    }
    public class UpdateRequestorObject
    {
        public int RequestorType { get; set; }
        public string RequestorName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public int WatsappEnabled { get; set; }
        public int Status { get; set; }
    }
}