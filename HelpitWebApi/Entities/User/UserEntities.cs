﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public string ExternalId { get; set; }
        public string UserName { get; set; }
        public string LoginId { get; set; }
        public int UserType { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string LastLoginDate { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class UserResponseObject : User
    {

    }

    public class AccessToken
    {
        public int Id { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public string Token { get; set; }
        public int CreatedByInt { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}