﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class IssueEntity
    {
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Priority { get; set; }
    }
}