﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.Entities
{
    public class Issue
    {
        public int Id { get; set; }
        public string IssueId { get; set; }
        public int OrganizationIntId { get; set; }
        public string OrganizationId { get; set; }
        public int ParentIntId { get; set; }
        public string ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResolutionNotes { get; set; }
        public string ResolutionDate { get; set; }
        public int CategoryIntId { get; set; }
        public string CategoryId { get; set; }
        public int SubCategoryIntId { get; set; }
        public string SubCategoryId { get; set; }
        public int RequestorIntId { get; set; }
        public string RequestorId { get; set; }
        public string Source { get; set; }
        public string Priority { get; set; }
        public string Tags { get; set; }
        public int OwnerIntId { get; set; }
        public string OwnerId { get; set; }
        public int AssignedToIntId { get; set; }
        public string AssignedToId { get; set; }
        public int UserType { get; set; }
        public int Status { get; set; }
        public int ObjectIntId { get; set; }
        public string ObjectId { get; set; }
        public string ObjectType { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class IssueRequestObject : Issue
    {

    }

    public class IssueResponseObject : Issue
    {
        public string IssueUserType { get; set; }
        public string IssueStatus { get; set; }
        public string RequestorName { get; set; }
        public string CreatedByName { get; set; }
    }

    public class UpdateIssueRequestObject 
    {
        public int Id { get; set; }
        public int Status { get; set; }
    }
}