﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class MasterListServiceBase : BaseServiceBase
    {
        MasterListServiceData _masterListService = new MasterListServiceData();
        public ServiceResponse GetCategories(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var categoriesList = _masterListService.GetCategories(userObject.OrganizationIntId, userObject.Id);
                if (categoriesList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(categoriesList), "Categories List Successfull", "Categories List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No List of Categories", "No List of Categories", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Master List Service Base", "GetCategories", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Categories List Failed", e.Message, "");
            }
        }

        public ServiceResponse GetStatus(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var statusList = _masterListService.GetStatus(userObject.OrganizationIntId, userObject.Id);
                if (statusList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(statusList), "Status List Successfull", "Status List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No List of Status", "No List of Status", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Master List Service Base", "GetStatus", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Status List Failed", e.Message, "");
            }
        }

        public ServiceResponse GetChannels(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var channelsList = _masterListService.GetChannels(userObject.OrganizationIntId, userObject.Id);
                if (channelsList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(channelsList), "Channels List Successfull", "Channels List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No List of Channels", "No List of Channels", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Master List Service Base", "GetChannels", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Channels List Failed", e.Message, "");
            }
        }
    }
}