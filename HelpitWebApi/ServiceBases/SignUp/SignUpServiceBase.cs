﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class SignUpServiceBase : BaseServiceBase
    {
        UserServiceData _userServiceData = new UserServiceData();
        OrganizationServiceData _orgServiceData = new OrganizationServiceData();
        public ServiceResponse Registration(RegisterRequestObject registerRequestObject)
        {
            try
            {
                var OrganizationId = Utility.NewId();
                var userId = Utility.NewId();
                var status = _userServiceData.IsUserExists(registerRequestObject.LoginId);
                if (status)
                    return ServiceResponseExtension.SeResponse("SUC-302", "Success", "", "LoginId already exists", "LoginId already exists", "");
                var OrganizationIntId = _orgServiceData.CreateOrganization(OrganizationId, registerRequestObject.OrganizationName, registerRequestObject.Description, registerRequestObject.Url,
                    1, userId, userId);
                if (!string.IsNullOrEmpty(OrganizationIntId))
                {
                    registerRequestObject.UserType = 0;
                    registerRequestObject.Status = 1;
                    registerRequestObject.OrganizationIntId = Convert.ToInt32(OrganizationIntId);
                    var UserName = string.Concat(registerRequestObject.FirstName, registerRequestObject.LastName);
                    int userStatus = _userServiceData.CreateUser(userId, OrganizationId, registerRequestObject.OrganizationIntId, "", UserName, registerRequestObject.LoginId,
                        registerRequestObject.Password, registerRequestObject.UserType, registerRequestObject.Status);
                    if (userStatus != 0)
                    {
                        return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "Registration Successful", "Registration Successful", "");
                    }
                }
                return ServiceResponseExtension.SeResponse("SUC-301", "Success", "", "Registration UnSuccessful-Unable to create company", "Registration UnSuccessful-Unable to create company", "");
            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, registerRequestObject.LoginId, 0, registerRequestObject.ToString(), "SignUp Service Base", "Registration", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Registration Failed", e.Message, "");
            }
        }

        public ServiceResponse VerifyEmail(string email)
        {
            var attachments = new List<Mailattachment>();
            char[] padding = { '=' };
            if (string.IsNullOrWhiteSpace(email))
                return ServiceResponseExtension.SeResponse("SUC-301", "Success", "", "Email can't be null or empty", "Email can't be null or empty", "");
            var emailTemplate = new TemplateServiceData().GetTemplate("VerifyEmail");

            try
            {
                var emailHtmlBody = new EmailServiceBase().GetMailBodywithoutfilepath(emailTemplate.HtmlTemplateBody, "");
                var mail = new EmailServiceBase().IntSendComplexMessage(email, "Outwork Support <support@outwork.ai>", new List<string>(),
                                new List<string>(), "", emailHtmlBody, emailTemplate.Subject, attachments.ToArray(), "");
                if (mail.StatusCode == "OK")
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", email, "Mail has been send to registered email id",
                        "Mail has been send to registered email id", "");
                }
            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, email, "SignUp Service Base", "Verify Email", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", email, "Mail Verification Failed", e.Message, "");
            }
            return ServiceResponseExtension.SeResponse("SUC-200", "Success", email, "Mail Verification Failed", "Mail Verification Failed", "");
        }
    }
}