﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class AdminRequestorServiceBase : BaseServiceBase
    {
        RequestorServiceData _requestorServiceData = new RequestorServiceData();
        public ServiceResponse CreateRequestorForAdmin(string userToken, RequestorRequestObject requestorRequest)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                int requestorId = _requestorServiceData.CreateRequestorForAdmin(requestorRequest, userObject.Id, userObject.UserId,
                    userObject.OrganizationIntId, userObject.OrganizationId);
                if (requestorId != 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", requestorId.ToString(), "Requestor Creation Successfull", "Requestor Creation Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Requestor Creation Failed", "Requestor Creation Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Requestor Service Base", "CreateRequestorForAdmin", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Requestor Creation Failed", e.Message, "");
            }
        }

        public ServiceResponse GetRequestorsForAdmin(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var requestorList = _requestorServiceData.GetRequestors(userObject.Id, userObject.OrganizationIntId);
                if (requestorList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(requestorList), "Requestor List Successfull", "Requestor List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Requestor List", "No Requestor List", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Requestor Service Base", "GetRequestorsForAdmin", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Requestor List Failed", e.Message, "");
            }
        }

        public ServiceResponse GetRequestorByRequestorId(string userToken, int requestorIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var requestorInfo = _requestorServiceData.GetRequestorByRequestorId(requestorIntId, userObject.Id, userObject.OrganizationIntId);
                if (requestorInfo != null)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(requestorInfo), "Requestor Info Successfull", "Requestor Info Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Requestor Info", "No Requestor Info", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Requestor Service Base", "GetRequestorByRequestorId", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Requestor Info Failed", e.Message, "");
            }
        }

        public ServiceResponse UpdateRequestorByRequestorId(string userToken, int requestorIntId, UpdateRequestorObject updateRequestor)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                bool updateStatus = _requestorServiceData.UpdateRequestorByRequestorId(requestorIntId, userObject.Id,
                    userObject.UserId, userObject.OrganizationIntId, updateRequestor);
                if (updateStatus)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", requestorIntId.ToString(), "Update Requestor Info Successfull", "Update Requestor Info Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Update Requestor Info Failed", "Update Requestor Info Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Requestor Service Base", "UpdateRequestorByRequestorId", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Update Requestor Failed", e.Message, "");
            }
        }
    }
}