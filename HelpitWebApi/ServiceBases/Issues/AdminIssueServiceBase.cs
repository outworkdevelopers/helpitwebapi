﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class AdminIssueServiceBase : BaseServiceBase
    {
        IssueServiceData _issueServiceData = new IssueServiceData();
        public ServiceResponse CreateIssueforAdmin(string userToken, IssueRequestObject issueRequest)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                int issueId = _issueServiceData.CreateIssue(issueRequest, userObject.Id, userObject.UserId, userObject.OrganizationId,
                    userObject.OrganizationIntId);
                if (issueId != 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", issueId.ToString(), "Issue Creation Successfull", "Issue Creation Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Issue Creation Failed", "Issue Creation Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Issue Service Base", "CreateIssueforAdmin", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Issue Creation Failed", e.Message, "");
            }
        }

        public ServiceResponse GetIssuesforAdmin(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var issuesList = _issueServiceData.GetIssues(userObject.OrganizationIntId, userObject.Id,userObject.UserId,userObject.UserName);
                if (issuesList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(issuesList), "Issue List Successfull", "Issue List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No List of Issues", "No List of Issues", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Issue Service Base", "GetIssuesforAdmin", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Issue List Failed", e.Message, "");
            }
        }

        public ServiceResponse GetIssueByIssueId(string userToken, int issueIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var issue = _issueServiceData.GetIssueByIssueId(userObject.OrganizationIntId, userObject.Id, issueIntId);
                if (issue != null)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(issue), "Issue Info Successfull", "Issue Info Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Info of Issue", "No Info of Issue", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Issue Service Base", "GetIssueByIssueId", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Issue Info Failed", e.Message, "");
            }
        }

        public ServiceResponse UpdateIssueStatusByIssueId(string userToken, int issueIntId, int status)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                bool issueStatus = _issueServiceData.UpdateIssueStatusByIssueId(issueIntId, status, userObject.Id, userObject.UserId, userObject.OrganizationIntId);
                var issue = _issueServiceData.GetIssueByIssueId(userObject.OrganizationIntId, userObject.Id, issueIntId);

                if (issueStatus)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", issueStatus.ToString(), "Update Issue Info Successfull", "Update Issue Info Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Update Issue Failed", "Update Issue Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Issue Service Base", "UpdateIssueStatusByIssueId", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Update Issue Info Failed", e.Message, "");
            }
        }


    }
}