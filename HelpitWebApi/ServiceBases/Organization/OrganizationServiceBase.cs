﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class OrganizationServiceBase : BaseServiceBase
    {
        readonly OrganizationServiceData _orgServiceData = new OrganizationServiceData();

        public OrganizationResponseObject GetOrganizationByUserId(int userIntId)
        {
            return _orgServiceData.GetOrganizationByUserId(userIntId);
        }
    }

}