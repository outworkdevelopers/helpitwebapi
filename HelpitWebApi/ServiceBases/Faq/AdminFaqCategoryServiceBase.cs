﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class AdminFaqCategoryServiceBase : BaseServiceBase
    {
        AdminFaqCategoryServiceData _faqCategoryServiceData = new AdminFaqCategoryServiceData();
        public ServiceResponse CreateFaqCategory(string userToken, FaqCategoryInputRequest faqCategoryObject)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                int faqCategoryId = _faqCategoryServiceData.CreateFaqCategory(faqCategoryObject, userObject.Id, userObject.UserId,
                    userObject.OrganizationIntId, userObject.OrganizationId);
                if (faqCategoryId != 0)
                {
                    var updateIntIdQuery = "update faqcategory set parentintid="+ faqCategoryId + " where id="+ faqCategoryId;
                    using (var connection = new MySqlConnection(DbConnectionString))
                    {
                        connection.Open();
                        using (var command = new MySqlCommand(updateIntIdQuery, connection))
                        {
                            var status=command.ExecuteNonQuery();
                        }
                    }
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", faqCategoryId.ToString(), "Faq Category Creation Successfull", "Faq Category Creation Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Faq Category Creation Failed", "Faq Category Creation Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "CreateFaqCategory", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Category Creation Failed", e.Message, "");
            }
        }

        public ServiceResponse GetFaqCategoryById(string userToken, int faqCategoryIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var faqCatgoryObject = _faqCategoryServiceData.GetFaqCategoryById(faqCategoryIntId, userObject.Id, userObject.OrganizationIntId);
                if (faqCatgoryObject != null)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(faqCatgoryObject), "Faq Category Successfull", "Faq Category Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Faq Category", "No Faq Category", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "GetFaqCategoryById", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Category Failed", e.Message, "");
            }
        }

        public ServiceResponse GetFaqCategories(string userToken)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var faqCategoriesList = _faqCategoryServiceData.GetFaqCategories(userObject.Id, userObject.OrganizationIntId);
                if (faqCategoriesList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(faqCategoriesList), "Faq Categories List Successfull", "Faq Categories  List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Faq Categories List", "No Faq Categories List", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "GetFaqCategories", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Categories List Failed", e.Message, "");
            }
        }

        public ServiceResponse UpdateFaqCategory(string userToken, int faqCategoryIntId, FaqCategoryInputRequest faqCategoryUpdate)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                bool faqUpdateStatus = _faqCategoryServiceData.UpdateFaqCategory(userObject.Id, userObject.UserId, userObject.OrganizationIntId, faqCategoryIntId, faqCategoryUpdate);
                if (faqUpdateStatus)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", faqCategoryIntId.ToString(), "Faq Category Update Successfull", "Faq Category Update Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Faq Category Update Failed", "Faq Category Update Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "UpdateFaqCategory", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Category Update Failed", e.Message, "");
            }
        }

        public ServiceResponse CreateTopic(string userToken, FaqCategoryInputRequest faqCategoryObject)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                int faqCategoryId = _faqCategoryServiceData.CreateTopic(faqCategoryObject, userObject.Id, userObject.UserId,
                    userObject.OrganizationIntId, userObject.OrganizationId);
                if (faqCategoryId != 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", faqCategoryId.ToString(), "Topic Creation Successfull", "Topic Creation Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Topic Creation Failed", "Topic Creation Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "CreateTopic", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Topic Creation Failed", e.Message, "");
            }

   
        }

        public ServiceResponse GetTopicByParentId(string userToken, int parentIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var faqCatgoryObject = _faqCategoryServiceData.GetTopicByParentId(parentIntId, userObject.Id, userObject.OrganizationIntId);
                if (faqCatgoryObject != null)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(faqCatgoryObject), "Topics Successfull", "Topics Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Topics", "No Topics", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Category Service Base", "GetTopicByParentId", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Get Topic By Parent Id Failed", e.Message, "");
            }
        }
    }
}