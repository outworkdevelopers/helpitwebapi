﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class AdminFaqServiceBase : BaseServiceBase
    {
        AdminFaqServiceData _faqServiceData = new AdminFaqServiceData();
        public ServiceResponse CreateFaq(string userToken, FaqInputObject faqObject)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                int faqId = _faqServiceData.CreateFaq(faqObject, userObject.Id, userObject.UserId,
                    userObject.OrganizationIntId, userObject.OrganizationId);
                if (faqId != 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", faqId.ToString(), "Faq Creation Successfull", "Faq Creation Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Faq Creation Failed", "Faq Creation Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Service Base", "CreateFaq", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Creation Failed", e.Message, "");
            }
        }

        public ServiceResponse GetFaqByFaqId(string userToken,string faqIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var faqObject = _faqServiceData.GetFaqByFaqId(faqIntId,userObject.Id, userObject.OrganizationIntId);
                if (faqObject!=null)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(faqObject), "Faq Successfull", "Faq Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Faq", "No Faq", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Service Base", "GetFaqByFaqId", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Failed", e.Message, "");
            }
        }

        public ServiceResponse GetFaqs(string userToken, int categoryIntId, int topicIntId)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                var faqsList = _faqServiceData.GetFaqs(userObject.Id, userObject.OrganizationIntId,categoryIntId,topicIntId);
                if (faqsList.Count > 0)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", JsonConvert.SerializeObject(faqsList), "Faqs List Successfull", "Faqs List Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("SUC-200", "Success", "", "No Faqs List", "No Faqs List", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Service Base", "GetFaqs", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faqs List Failed", e.Message, "");
            }
        }


        public ServiceResponse UpdateFaq(string userToken, int faqIntId, FaqInputObject faqUpdate)
        {
            try
            {
                var userObject = new UserServiceBase().GetUserByToken(userToken);
                bool faqUpdateStatus = _faqServiceData.UpdateFaq(userObject.Id, userObject.UserId,userObject.OrganizationIntId, faqIntId, faqUpdate);
                if (faqUpdateStatus)
                {
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", faqIntId.ToString(), "Faq Update Successfull", "Faq Update Successfull", "");
                }
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Faq Update Failed", "Faq Update Failed", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Admin Faq Service Base", "UpdateFaq", e.Message);
                return ServiceResponseExtension.SeResponse("", "Failed", "", "Faq Update Failed", e.Message, "");
            }
        }
    }
}