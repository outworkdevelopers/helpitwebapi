﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{
    public class LoginServiceBase : BaseServiceBase
    {
        readonly UserServiceData _userServiceData = new UserServiceData();
        readonly LoginServiceData _loginServiceData = new LoginServiceData();
        public ServiceResponse LoginUser(LoginRequestObject loginRequestObject)
        {
            var loggedInUser = _loginServiceData.LoginUser(loginRequestObject.LoginId, loginRequestObject.Password);
            try
            {
                var UserToken = new UserServiceBase().GetAuthenticationTicket(loggedInUser);
                var LoginResponse = GenerateLoginObject(loggedInUser, UserToken);
                if (!string.IsNullOrEmpty(UserToken))
                {
                    var token = _userServiceData.CreateUserToken(loggedInUser.Id, loggedInUser.UserId, LoginResponse.LoginType, LoginResponse.UserToken,
                                 1, "");
                }

                var response = JsonConvert.SerializeObject(LoginResponse);
                if (response != null)
                {

                    _userServiceData.UpdateLastLoginDate(loggedInUser.UserId);
                    return ServiceResponseExtension.SeResponse("SUC-200", "Success", response, "Login Success", "Login Success", "");

                }
                return ServiceResponseExtension.SeResponse("SUC-301", "Success", "", "Login-UserId/Password mismatch", "Login-UserId/Password mismatch", "");

            }

            catch (Exception e)
            {
                Common.CreateLog("", 0, loggedInUser.UserId, loggedInUser.Id, loginRequestObject.ToString(), "Login Service Base", "LoginUser", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Login Failed", e.Message, "");
            }
        }


        public LoginResponseObject GenerateLoginObject(UserResponseObject userInfo, string userToken)
        {
            var LoginResponse = new LoginResponseObject();

            var organizationInfo = new OrganizationServiceBase().GetOrganizationByUserId(userInfo.Id);

            var loginType = "";
            loginType = Utility.GetUserType(userInfo.UserType);
            LoginResponse = new LoginResponseObject
            {
                Id = userInfo.Id,
                UserId = userInfo.UserId,
                OrganizationId = userInfo.OrganizationId,
                OrganizationIntId = userInfo.OrganizationIntId,
                UserName = userInfo.UserName,
                ExternalId = userInfo.ExternalId,
                LoginId = userInfo.LoginId,
                UserType = userInfo.UserType,
                LoginType = loginType,
                Status = userInfo.Status,
                CreatedBy = userInfo.CreatedBy,
                ModifiedBy = userInfo.ModifiedBy,
                CreatedDate = userInfo.CreatedDate,
                ModifiedDate = userInfo.ModifiedDate,
                IsDeleted = userInfo.IsDeleted,
                UserToken = userToken,
                OrganizationInfo = organizationInfo

            };

            return LoginResponse;
        }

        public ServiceResponse JWTLoginUser(LoginRequestObject loginRequestObject)
        { 
            var loggedInUser = _loginServiceData.LoginUser(loginRequestObject.LoginId, loginRequestObject.Password);
            try
            {

                var UserToken = TokenManager.GetJWTAuthenticationTicket(loggedInUser);
                if (!string.IsNullOrWhiteSpace(UserToken))
                {
                    bool validateStatus = Validate(UserToken, loggedInUser);
                    if (validateStatus)
                    {
                        var LoginResponse = GenerateLoginObject(loggedInUser, UserToken);
                        var response = JsonConvert.SerializeObject(LoginResponse);
                        return ServiceResponseExtension.SeResponse("SUC-200", "Success", response, "Login Success", "Login Success", "");
                    }
                }
                return ServiceResponseExtension.SeResponse("SUC-301", "Success", "", "Login-UserId/Password mismatch", "Login-UserId/Password mismatch", "");

            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, loggedInUser.UserId, loggedInUser.Id, loginRequestObject.ToString(), "Login Service Base", "LoginUser", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Login Failed", e.Message, "");
            }
        }

        public bool Validate(string token, UserResponseObject objUser)
        {
            var userInfo = new UserServiceBase().GetUserByUserId(objUser.UserId);
            if (userInfo==null)
            {
                return false;
            }
            string tokenuserId= TokenManager.ValidateToken(token);
            if (objUser.UserId.Equals(tokenuserId))
            {
                return true;

            }
            else
                return false;

        }
    }
}