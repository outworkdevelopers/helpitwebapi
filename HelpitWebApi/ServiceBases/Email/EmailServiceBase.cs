﻿using HelpitWebApi.Entities;
using HelpitWebApi.Utilities;
using RazorEngine.Templating;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.WebPages.Razor.Configuration;

namespace HelpitWebApi.ServiceBases
{
    public class EmailServiceBase:BaseServiceBase
    {
        readonly string mailgunkey = ConfigurationManager.AppSettings["mailgunkey"];
        readonly string domainkey = ConfigurationManager.AppSettings["domainkey"];
        public MailRestResponse IntSendComplexMessage(string to, string from, List<string> cc, List<string> bcc, string body, string htmlbody, string subject,
            Mailattachment[] attachments, string accesstoken)
        {
            Log.Info("Int Send Complex Message : Mail Service Base: Execution Started");

            Dictionary<string, string> dicProp = new Dictionary<string, string>();
            dicProp.Add("read", "false");
            dicProp.Add("important", "false");
            dicProp.Add("attachments", "true");
            RestClient client = new RestClient();
            RestRequest request = new RestRequest();
            try
            {
                client.BaseUrl = new System.Uri("https://api.mailgun.net/v2");
                client.Authenticator = new HttpBasicAuthenticator("api", mailgunkey);
                request.AddParameter("domain", domainkey, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", from);
                request.AddParameter("to", to);
                foreach (string mailidcc in cc)
                {
                    request.AddParameter("cc", mailidcc);
                }
                foreach (string mailidbcc in cc)
                {
                    request.AddParameter("bcc", mailidbcc);
                }
                request.AddParameter("text", body);
                request.AddParameter("subject", subject);
                request.AddParameter("html", htmlbody);
                foreach (Mailattachment attachment in attachments)
                {
                    request.AddFile("attachment", attachment.Path);
                }
                request.Method = Method.POST;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                Log.Info("Int Send Complex Message : Mail Service Base: Execution Finished");
                Common.CreateLog("", 0, "", 0, "", "MailServiceBase", "IntSendComplexMessage", "");
            }
            Log.Info("Int Send Complex Message : Mail Service Base: Execution Finished");
            RestResponse simplemessagresponse = (RestResponse)client.Execute(request);
            return ConvertRestResponseToAppRestResponse(simplemessagresponse);
        }

        public static MailRestResponse ConvertRestResponseToAppRestResponse(RestResponse RestResponseOrigin)
        {
            MailRestResponse objAppRestResponse = new MailRestResponse
            {
                Content = RestResponseOrigin.Content,
                ContentEncoding = RestResponseOrigin.ContentEncoding,
                ContentLength = RestResponseOrigin.ContentLength,
                ContentType = RestResponseOrigin.ContentType,
                ErrorException = RestResponseOrigin.ErrorException,
                ErrorMessage = RestResponseOrigin.ErrorMessage,
                RawBytes = RestResponseOrigin.RawBytes,
                ResponseStatus = RestResponseOrigin.ResponseStatus.ToString(),
                ResponseUri = RestResponseOrigin.ResponseUri,
                Server = RestResponseOrigin.Server,
                StatusCode = RestResponseOrigin.StatusCode.ToString(),
                StatusDescription = RestResponseOrigin.StatusDescription
            };
            return objAppRestResponse;
        }

        public string GetMailBodywithoutfilepath(string templateFilePath, object model)
        {
            Log.Info("GetMailBodywithoutfilepath : Mail Service Base: Execution Started");
            var templateService = new TemplateService();
            templateService.AddNamespace("IvukoServiceAPI");
            var webConfigPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web.config");
            var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = webConfigPath };
            var configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            var razorConfig = configuration.GetSection("system.web.webPages.razor/pages") as RazorPagesSection;
            var emailHtmlBody = "";
            try
            {
                if (razorConfig != null)
                    foreach (NamespaceInfo namespaceInfo in razorConfig.Namespaces)
                    {
                        templateService.AddNamespace(namespaceInfo.Namespace);
                    }
            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Mail Service Base", "GetMailBodywithoutfilepath-razor", e.Message);
            }
            try
            {
                emailHtmlBody = templateService.Parse(templateFilePath, null, null, null);
                emailHtmlBody = HttpUtility.HtmlDecode(emailHtmlBody);
            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Mail Service Base", "GetMailBodywithoutfilepath", e.Message);
            }
            Log.Info("GetMailBodywithoutfilepath : Mail Service Base: Execution Finished");
            return emailHtmlBody;

        }
    }
}