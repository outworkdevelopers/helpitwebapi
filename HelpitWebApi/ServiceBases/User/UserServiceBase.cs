﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace HelpitWebApi.ServiceBases
{
    public class UserServiceBase : BaseServiceBase
    {
        readonly UserServiceData _userServiceData = new UserServiceData();
        public UserResponseObject GetUserByUserId(string userId)
        {
            return _userServiceData.GetUserByUserId(userId);
        }

        public string GetAuthenticationTicket(UserResponseObject objUser)
        {
            string token;
            if (!string.IsNullOrEmpty(objUser.UserId))
            {
                token = objUser.UserId + ":" + objUser.UserType + ":" + objUser.Status;
            }
            else
            {
                return "";
            }

            if (token != "")
            {
                var ticket = new FormsAuthenticationTicket(1, objUser.UserId, DateTime.Now, DateTime.Now.AddMinutes(2),
                         true, token, FormsAuthentication.FormsCookiePath);
                var encryptedTicket =
                     FormsAuthentication.Encrypt(ticket);
                var cookie = new HttpCookie
               (FormsAuthentication.FormsCookieName,
                encryptedTicket)
                { Expires = DateTime.Now.AddMinutes(2) };
                HttpContext.Current.Response.Cookies.Add(cookie);

                return encryptedTicket;
            }
            return "";
        }

        public UserResponseObject GetUserByToken(string userToken)
        {
            var loggedInUser = new UserResponseObject();
            var formsAuthenticationTicket = FormsAuthentication.Decrypt(userToken);
            if (formsAuthenticationTicket != null)
            {
                var token = formsAuthenticationTicket.UserData;

                loggedInUser.UserId = token.Split(new[] { ':' })[0];
            }

            try
            {
                var fetchedLoggedinUser = _userServiceData.GetUserByUserId(loggedInUser.UserId);
                if (fetchedLoggedinUser?.UserId != null)
                {
                    loggedInUser = fetchedLoggedinUser;
                }
            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, loggedInUser.UserId, loggedInUser.Id, userToken, "User Service Base", "GetUserByToken", e.Message);
                return loggedInUser;
            }
            return loggedInUser;

        }


        public AccessToken GetOrganizationByUserToken(string token)
        {
            var tokenDetails = new AccessToken();
            var accessTokenQuery = "select * from accesstoken where token=" + "'" + token + "'";
            using (var connection = new MySqlConnection(DbConnectionString))
            {
                connection.Open();
                using (var command = new MySqlCommand(accessTokenQuery, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            tokenDetails.Id = dataReader.ReadIntegerValue("id");
                            tokenDetails.OrganizationIntId = dataReader.ReadIntegerValue("organizationintid");
                            tokenDetails.OrganizationId = dataReader.ReadStringValue("organizationid");
                            tokenDetails.Token = dataReader.ReadStringValue("token");
                            tokenDetails.CreatedBy = dataReader.ReadStringValue("createdby");
                            tokenDetails.CreatedByInt = dataReader.ReadIntegerValue("createdbyint");
                            tokenDetails.CreatedDate = dataReader.ReadStringValue("createddate");
                        }
                    }
                }
            }
            return tokenDetails;
        }
    }
}