﻿using HelpitWebApi.Entities;
using HelpitWebApi.ServiceData;
using HelpitWebApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpitWebApi.ServiceBases
{

    public class SourceServiceBase : BaseServiceBase
    {
        SourceServiceData _sourceServiceData = new SourceServiceData();
        public ServiceResponse CreateIssueForRequestor(string userToken, IssueEntity issueEntity)
        {
            var tokenDetails = new UserServiceBase().GetOrganizationByUserToken(userToken);
            try
            {
                var phoneStatusId = _sourceServiceData.IsPhoneExists(issueEntity.PhoneNo);
                var emailStatusId = _sourceServiceData.IsEmailExists(issueEntity.EmailId);
                var singleRequestor = new RequestorResponseObject();
                if (phoneStatusId != 0)
                {
                    singleRequestor = new RequestorServiceData().GetRequestorByRequestorId(phoneStatusId, 0, tokenDetails.OrganizationIntId);
                }
                else if (emailStatusId != 0)
                {
                    singleRequestor = new RequestorServiceData().GetRequestorByRequestorId(emailStatusId, 0, tokenDetails.OrganizationIntId);

                }
                var issueInfo = new IssueRequestObject()
                {
                    OrganizationIntId = tokenDetails.OrganizationIntId,
                    OrganizationId = tokenDetails.OrganizationId,
                    Title = issueEntity.Title,
                    Description = issueEntity.Description,
                    RequestorIntId = singleRequestor.Id,
                    RequestorId = singleRequestor.RequestorId,
                    Source = issueEntity.Source,
                    Priority = issueEntity.Priority,
                    OwnerIntId = singleRequestor.UserIntId,
                    OwnerId = singleRequestor.UserId,
                    AssignedToId = singleRequestor.UserId,
                    AssignedToIntId = singleRequestor.UserIntId,
                    UserType = singleRequestor.RequestorType,
                    Status = 1,
                    ObjectIntId = tokenDetails.OrganizationIntId,
                    ObjectId = tokenDetails.OrganizationId,
                    ObjectType = "Organization",
                    CreatedBy = singleRequestor.RequestorId,
                    ModifiedBy = singleRequestor.RequestorId
                };

                if (phoneStatusId != 0 || emailStatusId != 0)
                {
                    if (singleRequestor.Status != 0 && singleRequestor.Status != 2)
                    {
                        var issueId = new IssueServiceData().CreateIssue(issueInfo, singleRequestor.UserIntId, singleRequestor.UserId,
                                singleRequestor.OrganizationId, singleRequestor.OrganizationIntId);
                        return ServiceResponseExtension.SeResponse("SUC-200", "Success", issueId.ToString(), "Issue Creation Successfull", "Issue Creation Successfull", "");

                    }
                    else
                        return ServiceResponseExtension.SeResponse("FAI-401", "FAILED", "", "Requestor Is Not Verified", "Requestor Is Not Verified", "");
                }
                else
                {
                    return ServiceResponseExtension.SeResponse("FAI-400", "FAILED", "", "Requestor Does Not Exists", "Requestor Does Not Exists", "");

                }



            }
            catch (Exception e)
            {
                Common.CreateLog("", 0, "", 0, "", "Source Service Base", "CreateIssueForRequestor", e.Message);
                return ServiceResponseExtension.SeResponse("FAI-400", "Failed", "", "Issue Creation Failed", e.Message, "");
            }
        }
    }
}